FROM alpine:edge

LABEL name="FPM RPM builder container for CI/CD"
LABEL maintainer="apozlevich@gmail.com"
LABEL version="0.1"

RUN apk add --no-cache gcc make musl-dev rpm ruby ruby-dev ruby-etc

RUN gem install --no-document fpm && gem cleanup all

RUN apk del --no-cache gcc musl-dev make
