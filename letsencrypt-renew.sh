#!/bin/bash

set -e

DEST_DIR=/etc/ssl/letsencrypt

usage() {
    echo "Usage: $(basename "$0") [OPTIONS...] -m EMAIL -d DOMAINS"
    echo
    echo "Obtains or renews Let's Encrypt SSL certificates without any user interaction."
    echo "Useful for automatization of certificates renewal without any configuration"
    echo "modifications made by Certbot."
    echo
    echo "Currently works only with nginx authentication."
    echo
    echo "Options:"
    echo
    echo "  -h --help             Print this help and exit"
    echo "  -v --verbose          Print executed commands (set -x)"
    echo "  -q --quiet            Execute quietly"
    echo
    echo "  -e --email            Email address for important account notifications"
    echo "  -d --domains          Comma-separated list of domains"
    echo "  -c --no-copy          Don't copy certificates from /etc/letsencrypt/live"
    echo "  -D --destination DIR  Destination to copy certs to. Default is "
    echo "                        /etc/ssl/letsencrypt"
    echo
    echo "  -o --chown OWNER      Change owner of certificates"
    echo "  -m --chmod MODE       Change file mode of certificates"
    echo
    echo "     --staging          Obtain a test certificate from a staging server"
    echo "     --dry-run          Test without saving any certificates"
    echo
}

verbose() {
    [ -v VERBOSE ] && set -x
    $*
    { [ -v VERBOSE ] && set +x; } 2>/dev/null
}

while [[ $# -gt 0 ]] ; do
    case "$1" in
        -h|--help)
        usage
        exit
        ;;

        -v|--verbose)
        VERBOSE=YES
        shift
        ;;

        -q|--quiet)
        QUIET=YES
        shift
        ;;

        -e|--email)
        EMAIL="$2"
        shift
        shift
        ;;

        -d|--domains)
        DOMAINS="$2"
        shift
        shift
        ;;

        -c|--no-copy)
        NO_COPY=YES
        shift
        ;;

        -D|--destination)
        DEST_DIR="$2"
        shift
        shift
        ;;

        -o|--chown)
        DEST_OWNER="$2"
        shift
        shift
        ;;

        -m|--chmod)
        DEST_MODE="$2"
        shift
        shift
        ;;

        --staging)
        STAGING=YES
        shift
        ;;

        --dry-run)
        DRY_RUN=YES
        shift
        ;;

        *)
        usage
        exit 1
        ;;
    esac
done

[ -v DRY_RUN ] && DRY_RUN=" --dry-run" 
[ -v STAGING ] && STAGING=" --test-cert"

if [ ! -v EMAIL ] || [ ! -v DOMAINS ] ; then
    usage
    exit 1
fi

if [[ $(id -u) != 0 ]] ; then
    echo 'This script meant to run as root.'
    exit 1
fi

if [ ! -f "/bin/certbot" ] ; then
    echo 'Certbot seems to be missing.'
    exit 1
fi

[ ! -v DRY_RUN ] && [ ! -d "$DEST_DIR" ] && verbose mkdir -p "$DEST_DIR"

if [ -v QUIET ] ; then
    [ -v VERBOSE ] && >&2 echo "+ certbot certonly -n --nginx --agree-tos$DRY_RUN$STAGING -m "$EMAIL" -d $DOMAINS"
    /bin/certbot certonly -n --nginx --agree-tos$DRY_RUN$STAGING -m "$EMAIL" -d $DOMAINS > /dev/null 2> /dev/null
else
    verbose /bin/certbot certonly -n --nginx --agree-tos$DRY_RUN$STAGING -m "$EMAIL" -d $DOMAINS 2>&1 > /dev/null
fi

if [ ! -v DRY_RUN ] ; then
    for certdir in /etc/letsencrypt/live/*/ ; do
        certname=$(basename $certdir)
        verbose cp -Lf /etc/letsencrypt/live/$certname/fullchain.pem $DEST_DIR/$certname.fullchain.pem
        verbose cp -Lf /etc/letsencrypt/live/$certname/privkey.pem $DEST_DIR/$certname.privkey.pem
    done

    [ -v DEST_OWNER ] && verbose chown -R $DEST_OWNER "$DEST_DIR"
    [ -v DEST_MODE ] && verbose chmod -R $DEST_MODE "$DEST_DIR"
fi
