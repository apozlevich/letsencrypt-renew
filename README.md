# letsencrypt-renew

Obtains or renews Let's Encrypt SSL certificates without any user interaction. Useful for automatization of certificates renewal without any annoying configuration modifications made by Certbot.

## Script usage

```text
Usage: letsencrypt-renew [OPTIONS...] -m EMAIL -d DOMAINS

Obtains or renews Let's Encrypt SSL certificates without any user interaction.
Useful for automatization of certificates renewal without any configuration
modifications made by Certbot.

Currently works only with nginx authentication.

Options:

  -h --help             Print this help and exit
  -v --verbose          Print executed commands (set -x)
  -q --quiet            Execute quietly

  -e --email            Email address for important account notifications
  -d --domains          Comma-separated list of domains
  -c --no-copy          Don't copy certificates from /etc/letsencrypt/live
  -D --destination DIR  Destination to copy certs to. Default is
                        /etc/ssl/letsencrypt

  -o --chown OWNER      Change owner of certificates
  -m --chmod MODE       Change file mode of certificates

     --staging          Obtain a test certificate from a staging server
     --dry-run          Test without saving any certificates
```

## Systemd usage

RPM package installs systemd timer and service units. Configure it by edit `/usr/local/etc/letsencrypt-renew`:

```shell
EMAIL=webmaster@domain.tld         # Address to receive information mail
DOMAINS=domain.tld,www.domain.tld  # Comma-separated domains list
DESTINATION=/etc/ssl/letsencrypt   # Directory to copy certificates to
CHOWN=nginx:nginx                  # File owner for certificates
CHMOD=660                          # File mode for certificates
```

And enable timer unit by executing:

```shell
sudo systemctl enable letsencrypt-renew.timer
sudo systemctl start letsencrypt-renew.timer
```

You can get certificates immediately by running service unit:

```shell
sudo systemctl start letsencrypt-renew.service
```

## Building RPM with Docker

~~Just for fun~~ You can build RPM package by yourself using [Effing Package Management](https://github.com/jordansissel/fpm/wiki) using arguments provided in `docker-compose.yml` or just by using `docker-compose up --build`. Docker will create directory named `output` containing newly-generated RPM package.
